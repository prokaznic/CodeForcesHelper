﻿namespace CodeForcesHelper
{
    partial class ProblemLoader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRun = new System.Windows.Forms.Button();
            this.TargetProblem = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(278, 163);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(107, 28);
            this.btnRun.TabIndex = 0;
            this.btnRun.Text = "btnRun";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // TargetProblem
            // 
            this.TargetProblem.Location = new System.Drawing.Point(24, 24);
            this.TargetProblem.Name = "TargetProblem";
            this.TargetProblem.Size = new System.Drawing.Size(320, 20);
            this.TargetProblem.TabIndex = 1;
            this.TargetProblem.Text = "http://codeforces.com/problemset/problem/1/A";
            // 
            // ProblemLoader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 262);
            this.Controls.Add(this.TargetProblem);
            this.Controls.Add(this.btnRun);
            this.Name = "ProblemLoader";
            this.Text = "ProblemLoader";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.TextBox TargetProblem;
    }
}