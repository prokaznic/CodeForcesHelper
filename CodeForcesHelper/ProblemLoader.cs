﻿using System;
using System.Windows.Forms;

namespace CodeForcesHelper {
    public partial class ProblemLoader : Form {
        public ProblemLoader(Main mainForm) {
            InitializeComponent();
            _mainForm = mainForm;
        }

        private readonly Main _mainForm;

        private async void btnRun_Click(object sender, EventArgs e) {
            _mainForm.Problem = await Problem.BuildAsync(TargetProblem.Text);
            Dispose();
        }
    }
}
