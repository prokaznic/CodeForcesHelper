﻿using System;
using System.Windows.Forms;

namespace CodeForcesHelper {
    public partial class Main : Form {
        public Main() {
            InitializeComponent();
        }

        private Problem _problem;
        public Problem Problem {
            get { return _problem; }
            set {
                _problem = value;
                Title.Text = $"{_problem.Number} {_problem.Letter}: {_problem.Title}";
                Memory.Text = _problem.Memory.ToString();
                Time.Text   = _problem.Time.ToString();
            }
        }

        private void Main_Load(object sender, EventArgs e) {
            ProblemLoader problemLoader = new ProblemLoader(this);
            problemLoader.ShowDialog();
        }

    }
}
