﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace CodeForcesHelper {
    public class Problem {
        private Problem() { }

        public string Title  { private set; get; }
        public string Source { private set; get; }
        public ushort Time   { private set; get; }
        public ushort Memory { private set; get; }
        public Uri    Uri    { private set; get; }
        public char   Letter { private set; get; }
        public ushort Number { private set; get; }

        private static HtmlAgilityPack.HtmlDocument _htmlDoc;


        public static Problem Build(string s) {
            var uri    = UriGenerate(s);
            var source = Download(uri);

            _htmlDoc = new HtmlAgilityPack.HtmlDocument();
            _htmlDoc.OptionFixNestedTags = true;
            _htmlDoc.LoadHtml(source);

            var time   = ParseTime();
            var memory = ParseMemory();

            Console.WriteLine($"Задача {uri.AbsolutePath} загружена");

            return new Problem {
                Title  = "TITLE",
                Source = source,
                Uri    = uri,
                Memory = memory,
                Letter = 'Z',
                Number = 999,
                Time   = time
            };
        }

        public static Task<Problem> BuildAsync(string s) {
            return Task.Run(() => Build(s));
        }


        private static Uri UriGenerate(string s) {
            return new Uri(s);
        }

        private static string Download(Uri problemUri) {

            HttpWebRequest req = (HttpWebRequest)(
                (WebRequest.Create(problemUri)).Proxy = new WebProxy("127.0.0.1:8118")
            );
        
            HttpWebResponse resp = null;
            try {
                resp = (HttpWebResponse)req.GetResponse();
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            string sourceCode;
            using (StreamReader stream = new StreamReader(resp.GetResponseStream(), Encoding.UTF8)) {
                sourceCode = stream.ReadToEnd();
            }

            return sourceCode;
        }

        private static ushort ParseTime() {
            HtmlNode testsNode = _htmlDoc.DocumentNode.SelectNodes("//div[@class='time-limit']").First();
            var time = testsNode.LastChild.InnerText.Split().First();
            return Convert.ToUInt16(time);
        }

        private static ushort ParseMemory() {
            HtmlNode testsNode = _htmlDoc.DocumentNode.SelectNodes("//div[@class='memory-limit']").First();
            var time = testsNode.LastChild.InnerText.Split().First();
            return Convert.ToUInt16(time);
        }
    }
}